package com.waykichain.controller

import com.waykichain.config.Constant
import com.waykichain.model.*
import com.waykichain.program.VersionPro
import com.waykichain.service.ChainXService
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.context.request.NativeWebRequest
import java.lang.Error
import java.util.*
import javax.validation.Valid
import kotlin.collections.HashMap

@Controller
class NetworkApiController(val request: NativeWebRequest) {
    @Autowired(required = false)
    lateinit var chainXService: ChainXService
    fun getRequest(): Optional<NativeWebRequest> {
        return Optional.ofNullable(request)
    }


    @ApiOperation(value = "Get List of Available Networks", nickname = "networkList", notes = "This endpoint returns a list of NetworkIdentifiers that the Rosetta server supports.", response = NetworkListResponse::class, tags = ["Network"])
    @ApiResponses(value = [ApiResponse(code = 200, message = "Expected response to a valid request", response = NetworkListResponse::class), ApiResponse(code = 200, message = "unexpected error", response = Error::class)])
    @RequestMapping(value = ["/network/list"], produces = ["application/json"], consumes = ["application/json"], method = [RequestMethod.POST])
    fun networkList(@ApiParam(value = "", required = true) @RequestBody metadataRequest: @Valid MetadataRequest?): ResponseEntity<NetworkListResponse> {
        val networkListResponse = NetworkListResponse()
        getRequest().ifPresent { request: NativeWebRequest ->
            for (mediaType in MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    val networkIdentifier = NetworkIdentifier()
                    networkIdentifier.blockchain = "waykichain"
                    networkIdentifier.network = "mainnet"
                    networkListResponse.addNetworkIdentifiersItem(networkIdentifier)
                    break
                }
            }
        }
        return ResponseEntity(networkListResponse, HttpStatus.OK)
    }


    @ApiOperation(value = "Get Network Options", nickname = "networkOptions", notes = "This endpoint returns the version information and allowed network-specific types for a NetworkIdentifier. Any NetworkIdentifier returned by /network/list should be accessible here. Because options are retrievable in the context of a NetworkIdentifier, it is possible to define unique options for each network.", response = NetworkOptionsResponse::class, tags = ["Network"])
    @ApiResponses(value = [ApiResponse(code = 200, message = "Expected response to a valid request", response = NetworkOptionsResponse::class), ApiResponse(code = 200, message = "unexpected error", response = Error::class)])
    @RequestMapping(value = ["/network/options"], produces = ["application/json"], consumes = ["application/json"], method = [RequestMethod.POST])
    fun networkOptions(@ApiParam(value = "", required = true) @RequestBody networkRequest: @Valid NetworkRequest?): ResponseEntity<NetworkOptionsResponse> {
        val networkOptionsResponse = NetworkOptionsResponse()
        networkOptionsResponse.version = com.waykichain.model.Version()
                .rosettaVersion(Constant.rosettaVersion)
                .nodeVersion(VersionPro.getVersion())
                .middlewareVersion(Constant.middlewareVersion)
        val allow = Allow()
        allow.historicalBalanceLookup = false
        // allow.setHistoricalBalanceLookup(true);
        allow.operationTypes = Arrays.asList(*Constant.supportOperationTypes)
        allow.operationStatuses = Constant.supportOperationStatuses
        allow.errors = Constant.supportErrors
        networkOptionsResponse.allow = allow
        return ResponseEntity(networkOptionsResponse, HttpStatus.OK)
    }


    @ApiOperation(value = "Get Network Status", nickname = "networkStatus", notes = "This endpoint returns the current status of the network requested. Any NetworkIdentifier returned by /network/list should be accessible here.", response = NetworkStatusResponse::class, tags = ["Network"])
    @ApiResponses(value = [ApiResponse(code = 200, message = "Expected response to a valid request", response = NetworkStatusResponse::class), ApiResponse(code = 200, message = "unexpected error", response = Error::class)])
    @RequestMapping(value = ["/network/status"], produces = ["application/json"], consumes = ["application/json"], method = [RequestMethod.POST])
    fun networkStatus(@ApiParam(value = "", required = true) @RequestBody networkRequest: @Valid NetworkRequest?): ResponseEntity<NetworkStatusResponse> {
        val networkStatusResponse = NetworkStatusResponse()
        val genesisBlock = chainXService.findBlockBean(0)
        networkStatusResponse.genesisBlockIdentifier = BlockIdentifier()
                .index(0.toLong())
                .hash(genesisBlock.block.curr_block_hash)
        val index = chainXService.getMaxBlockNumber()
        val currentBlock = chainXService.findBlockBean(index.toInt())
        networkStatusResponse.currentBlockIdentifier = BlockIdentifier()
                .index(index)
                .hash(currentBlock.block.curr_block_hash)
        networkStatusResponse.currentBlockTimestamp = currentBlock.block.time


        val peers= arrayListOf<Peer>()
        for (peer in chainXService.getPeerInfo().result) {
            val map =HashMap<String, Any>()
            val addr = peer.addr.split(":")
            map.put("address", addr[0])
            map.put("port", addr[1])
            peers.add(Peer().metadata(map))
        }
        networkStatusResponse.peers(peers)
        return ResponseEntity(networkStatusResponse, HttpStatus.OK)
    }
}