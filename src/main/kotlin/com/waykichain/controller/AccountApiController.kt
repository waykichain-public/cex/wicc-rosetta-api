package com.waykichain.controller

import com.alibaba.fastjson.JSON
import com.waykichain.common.ApiUtil
import com.waykichain.config.Constant
import com.waykichain.model.*
import com.waykichain.model.Currency
import com.waykichain.service.ChainXService
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.context.request.NativeWebRequest
import java.util.*
import javax.security.auth.login.AccountException
import javax.validation.Valid


@Controller
class AccountApiController @Autowired constructor(private val request: NativeWebRequest) {
    @Autowired
    private val chainXService: ChainXService? = null

    private fun getRequest(): Optional<NativeWebRequest> {
        return Optional.ofNullable(request)
    }

    @ApiOperation(value = "Get an Account Balance", nickname = "accountBalance", notes = "Get an array of all AccountBalances for an AccountIdentifier and the BlockIdentifier at which the balance lookup was performed. The BlockIdentifier must always be returned because some consumers of account balance data need to know specifically at which block the balance was calculated to compare balances they compute from operations with the balance returned by the node. It is important to note that making a balance request for an account without populating the SubAccountIdentifier should not result in the balance of all possible SubAccountIdentifiers being returned. Rather, it should result in the balance pertaining to no SubAccountIdentifiers being returned (sometimes called the liquid balance). To get all balances associated with an account, it may be necessary to perform multiple balance requests with unique AccountIdentifiers. It is also possible to perform a historical balance lookup (if the server supports it) by passing in an optional BlockIdentifier.", response = AccountBalanceResponse::class, tags = ["Account"])
    @ApiResponses(value = [ApiResponse(code = 200, message = "Expected response to a valid request", response = AccountBalanceResponse::class), ApiResponse(code = 200, message = "unexpected error", response = java.lang.Error::class)])
    @RequestMapping(value = ["/account/balance"], produces = ["application/json"], consumes = ["application/json"], method = [RequestMethod.POST])
    fun accountBalance(
            @ApiParam(value = "", required = true) @RequestBody accountBalanceRequest: @Valid AccountBalanceRequest?): ResponseEntity<AccountBalanceResponse> {
        if (getRequest().isPresent) {
            for (mediaType in MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    val response: AccountBalanceResponse
                    var partialBlockIdentifier: PartialBlockIdentifier? = null
                    try {
                        partialBlockIdentifier = accountBalanceRequest?.blockIdentifier
                        response = if (partialBlockIdentifier == null ||
                                partialBlockIdentifier.hash == null &&
                                partialBlockIdentifier.index == null ) {
                            getCurrentBalance(accountBalanceRequest)
                        } else {
                            getHistoryBalance(accountBalanceRequest)
                        }
                    } catch (e: Exception) {
                        val error = Error()
                                .code(Constant.BLOCK_IS_NOT_EXISTS.code)
                                .message(Constant.BLOCK_IS_NOT_EXISTS.message)
                                .retriable(false)
                                .details(partialBlockIdentifier)
                        return ApiUtil.sendError(request, JSON.toJSONString(error))
                    }
                    val headers = HttpHeaders()
                    headers.contentType = MediaType.APPLICATION_JSON_UTF8
                    return ResponseEntity(response, headers, HttpStatus.OK)
                }
            }
        }
        return ResponseEntity(HttpStatus.valueOf(200))
    }

    @Throws(AccountException::class)
    fun getCurrentBalance(accountBalanceRequest: AccountBalanceRequest?): AccountBalanceResponse {
        val accountIdentifier = accountBalanceRequest!!.accountIdentifier ?: throw AccountException()
        val address = accountIdentifier.address
        val regid = chainXService!!.getAccountInfo(address)!!.regid
        val index = regid.split("-")[0]
        val blockIdentifier = BlockIdentifier()
        blockIdentifier.index(index.toLong())
                .hash(chainXService.findBlockBean(index.toInt()).block.curr_block_hash)
        val rep = AccountBalanceResponse()
        rep.blockIdentifier(blockIdentifier).balances(getTokenBalance(address))
        return rep
    }

    @Throws(AccountException::class, Exception::class)
    fun getHistoryBalance(accountBalanceRequest: AccountBalanceRequest?): AccountBalanceResponse {
        val accountIdentifier = accountBalanceRequest!!.accountIdentifier ?: throw AccountException()
        val partialBlockIdentifier = accountBalanceRequest.blockIdentifier
        var index = partialBlockIdentifier.index
        var hash = partialBlockIdentifier.hash

        if ((index == null || index <= 0) && StringUtils.isEmpty(hash)) throw Exception()
        if (index == null || index <= 0) index = chainXService!!.getBlockByHash(hash!!).height.toLong()
        if (StringUtils.isEmpty(hash)) hash = chainXService!!.findBlockBean(index.toInt()).block.curr_block_hash

        val blockIdentifier = BlockIdentifier().index(index).hash(hash)
        val address = accountIdentifier.address
        if (StringUtils.isEmpty(address)) throw AccountException()
        val rep = AccountBalanceResponse()
        rep.blockIdentifier(blockIdentifier).balances(getTokenBalance(address))
        return rep
    }

    private fun getTokenBalance(address: String?): List<Amount> {
        val amounts: MutableList<Amount> = ArrayList()
        val response = chainXService!!.getAccountInfo(address!!)
        val tokens = response!!.tokens
        for ((key, value) in tokens) {
            val amount = Amount()
            val currency = Currency().decimals(8).symbol(key)
            amount.value(value.total_amount.toString()).currency(currency)
            amounts.add(amount)
        }
        return amounts
    }
}