package com.waykichain.config

import com.waykichain.com.waykichain.env.swagger.Environment
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.context.request.async.DeferredResult
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket



@Configuration
open class SwaggerConfig {

    @Bean
    open fun api(): Docket = Docket(DocumentationType.SWAGGER_2)
            .host(Environment
                    .SWAGGER_WAPAPI_HOST)
            .enable(Environment.WICC_SERVICE_ENABLE_SWAGGER)
            .groupName("wicc-rosetta-api")
            .genericModelSubstitutes(DeferredResult::class.java)
            .useDefaultResponseMessages(false)
            .forCodeGeneration(false)
            .pathMapping("/")
            .select()
                .apis(RequestHandlerSelectors.basePackage("com.waykichain"))
                .build()
            .apiInfo(loanApiInfo())

    private fun loanApiInfo(): ApiInfo {
        return ApiInfoBuilder()
                .title(Environment.SWAGGER_WABAPI_TITLE)//大标题
                .description(Environment.SWAGGER_WABAPI_DESCRIPTION)//详细描述
                .version("1.0")//版本
                .termsOfServiceUrl(Environment.SWAGGER_WABAPI_SERVICEURL)
                .license(Environment.SWAGGER_WABAPI_LICENSE)
                .licenseUrl(Environment.SWAGGER_WABAPI_LICENSE_URL)
                .build()

    }
}