package com.waykichain.com.waykichain.service

import com.waykichain.JsonRpcClient
import com.waykichain.coin.wicc.WiccMethods
import com.waykichain.com.waykichain.env.coin.Environment.Environment
import org.slf4j.LoggerFactory

class RpcMethodClient {
    companion object {
        @JvmStatic
        val clients = ArrayList<JsonRpcClient>()
    }

    @Synchronized
    fun getClient(): WiccMethods {
        logger.info("btc client ip:${Environment.WICC_HOST_IP} port:${Environment.WICC_HOST_PORT}")
        if (clients.isEmpty()) {
            clients.add(client())
        }
        return WiccMethods(clients.first())
    }

    private fun client(): JsonRpcClient {
        return JsonRpcClient(
                Environment.WICC_HOST_IP,
                Environment.WICC_HOST_PORT,
                Environment.WICC_RPC_USERNAME,
                Environment.WICC_RPC_PASSWORD,
                false)
    }

    private val logger = LoggerFactory.getLogger(javaClass)
}