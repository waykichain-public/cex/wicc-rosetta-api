package com.waykichain.service

import com.alibaba.fastjson.JSONObject
import com.google.gson.Gson
import com.waykichain.ChainException
import com.waykichain.bean.BlockBean
import com.waykichain.chain.dict.SysCoinType
import com.waykichain.chain.dict.TransactionType
import com.waykichain.chainstarter.client.WiccMethodClient
import com.waykichain.chainstarter.service.WaykichainCoinHandler
import com.waykichain.coin.wicc.po.GetAssetPO
import com.waykichain.coin.wicc.po.GetContractInfoPO
import com.waykichain.coin.wicc.po.GetWasmAbiPO
import com.waykichain.coin.wicc.po.RawSignaturePO
import com.waykichain.coin.wicc.vo.*
import com.waykichain.coin.wicc.vo.tx.BaseTx
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.util.*
import kotlin.collections.ArrayList

@Service
class ChainXService {
    private val logger = LoggerFactory.getLogger(javaClass)

    fun findBlockBean(height: Int): BlockBean {
        val block = chainHandler.getBlockByHeight(height)
        val txs = arrayListOf<BaseTx>()
        block.tx.forEach {
            txs.add(chainHandler.getTxDetail(it)!!)
        }

        return BlockBean(block, txs)

    }


    fun getAccountInfo(w: String): WiccAccountInfoResult? {
        val account: WiccAccountInfoResult?
        try {
            account = chainHandler.getAccountInfo(w)
        } catch (e: Exception) {
            logger.error("pull wallet error", e)
            return null
        }
        return account
    }

    fun getWiccBalance(tokens: HashMap<String, WiccAccountInfoResult.AccountTokenInfo>): BigDecimal {
        return calBalance(((tokens[SysCoinType.WICC.code]?.free_amount) ?: 0).toBigDecimal())
    }

    fun getWusdBalance(tokens: HashMap<String, WiccAccountInfoResult.AccountTokenInfo>): BigDecimal {
        return calBalance(((tokens[SysCoinType.WUSD.code]?.free_amount) ?: 0).toBigDecimal())
    }

    fun getWgrtBalance(tokens: HashMap<String, WiccAccountInfoResult.AccountTokenInfo>): BigDecimal {
        return calBalance(((tokens[SysCoinType.WGRT.code]?.free_amount) ?: 0).toBigDecimal())
    }


    fun getCoinBalance(tokens: HashMap<String, WiccAccountInfoResult.AccountTokenInfo>, coinSymbol: String?): BigDecimal {
        return calBalance(((tokens[coinSymbol]?.free_amount) ?: 0).toBigDecimal())
    }

    fun getAccountTokenBalance(address: String, contractRegid: String): BigDecimal {
        return calBalance(chainHandler.getContractAccountinfo(address, contractRegid)?.free_value!!.toBigDecimal())
    }

    fun getAsset(po: GetAssetPO): GetAssetResult? {
        var assert = GetAssetResult()
        try {
            assert = chainHandler.getAsset(po)
        } catch (e: Exception) {
            logger.error("getAsset error", e)
            return null
        }
        return assert
    }

    fun getContractRegId(hash: String): String? {
        return chainHandler.getContractRegid(hash)
    }

    fun getContractContent(contractRegid: String?): String? {
        contractRegid ?: return null
        return chainHandler.getContractContent(contractRegid)
    }


    private fun calBalance(balance: BigDecimal): BigDecimal {
        return balance.divide(100000000.toBigDecimal(), 8, BigDecimal.ROUND_HALF_UP)
    }

    fun getWasmAbi(regid: String?): JSONObject {
        var po = GetWasmAbiPO()
        po.contract = regid
        var reponse = client().getwasmabi(po)
        return reponse.result.abi
    }

    fun getVmType(regid: String?): Int {
        var po = GetContractInfoPO()
        po.regid = regid
        val reponse = client().getContractInfo(po)
        return reponse.result.vm_type
    }

    fun getBlockByHash(hash: String): WiccBlockResult {
        val blockResult = client().getBlock(hash) ?: throw  NullPointerException("response must not be null")
        blockResult.validate()
        return blockResult.result
    }

    fun getMaxBlockNumber(): Long {
//        return client().info.result.tipblock_height.toLong()
        return client().finBlockCount.result
    }

    fun getPeerInfo(): WiccGetPeerInfoResultResponse {
        return client().peerInfo
    }

    fun getTxDetail(txHash: String): BaseTx? {
        val txJsonString = client().getTxDetailJson(txHash) ?: return null
        val txObject = JSONObject.parseObject(txJsonString).getJSONObject("result")
        val txType = txObject.getString("tx_type") ?: return null
        return transferTx(txObject.toJSONString(), TransactionType.parseTransactionTypeToTxClass(txType))
    }

    fun submitTxRaw(rawTx: String, sinatures: ArrayList<RawSignaturePO>?): String {
        return client().submittxraw(rawTx, sinatures).txid
    }

    fun getSignTxRawResult(unsignedRaw: String, signAddr: ArrayList<String>):WiccSignTxRawResult{
        return wiccMethodClient.getClient().getSignTxRawResult(unsignedRaw,signAddr)
    }

    private fun <T : BaseTx> transferTx(txString: String, txClass: Class<T>): T {

        return Gson().fromJson(txString, txClass)
    }

    @Autowired
    lateinit var chainHandler: WaykichainCoinHandler

    private fun client() = wiccMethodClient.getClient()

    @Autowired
    lateinit var wiccMethodClient: WiccMethodClient

}