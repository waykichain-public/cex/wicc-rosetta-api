package com.waykichain.bean

import java.io.Serializable
import java.math.BigDecimal
import java.util.*


class WiccBlock : Serializable {
    val chainwork: String? = null
    val confirmations: Int? = null
    val createdAt: Date? = null
    val fuel: BigDecimal? = null
    val fuelRate: Int? = null
    val hash: String? = null
    val height: Int? = null
    val id: Long? = null
    val merkleRoot: String? = null
    val minerAddress: String? = null
    val minerName: String? = null
    val nextBlockHash: String? = null
    val nonce: Long? = null
    val previousBlockHash: String? = null
    val producedAt: Date? = null
    val size: Int? = null
    val transactionAmount: BigDecimal? = null
    val transactionCount: Int? = null
    val transactionFees: BigDecimal? = null
    val updatedAt: Date? = null
    val version: Int? = null
}