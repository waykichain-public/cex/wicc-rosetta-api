package com.waykichain.bean

import com.waykichain.coin.wicc.vo.WiccBlockResult
import com.waykichain.coin.wicc.vo.tx.BaseTx
import com.waykichain.bean.WiccBlock


/**
 *  Created by yehuan on 2019/4/9
 */

class BlockBean(var block: WiccBlockResult, var transactions: List<BaseTx>, var oldBlock: WiccBlock? = null)