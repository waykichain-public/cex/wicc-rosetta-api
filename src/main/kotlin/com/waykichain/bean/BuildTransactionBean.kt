package com.waykichain.com.waykichain.bean

import lombok.AllArgsConstructor
import lombok.Data
import lombok.NoArgsConstructor


@Data
class BuildTransactionBean(srcPubKey: String, commonTransactionRaw: String) {
    var srcPubKey: String? = null
    var commonTransactionRaw: String? = null
    init {
        this.srcPubKey=srcPubKey
        this.commonTransactionRaw=commonTransactionRaw
    }
}
