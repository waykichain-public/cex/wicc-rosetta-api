package com.waykichain.com.waykichain.env.swagger

import com.waykichain.commons.util.BaseEnv


object Environment {

    @JvmField
    var SWAGGER_WAPAPI_HOST = BaseEnv.env("SWAGGER_WAPAPI_HOST", "127.0.0.1")

    @JvmField
    var WICC_SERVICE_ENABLE_SWAGGER = BaseEnv.env("WICC_SERVICE_ENABLE_SWAGGER", true)

    @JvmField
    var SWAGGER_WABAPI_TITLE = BaseEnv.env("SWAGGER_WABAPI_TITLE", "Rosetta")


    @JvmField
    var SWAGGER_WABAPI_DESCRIPTION = BaseEnv.env("SWAGGER_WABAPI_DESCRIPTION", "Build Once. Integrate Your Blockchain Everywhere.")

    @JvmField
    var SWAGGER_WABAPI_VERSION = BaseEnv.env("SWAGGER_WABAPI_VERSION", "1.0")



    @JvmField
    var SWAGGER_WABAPI_SERVICEURL = BaseEnv.env("SWAGGER_WABAPI_SERVICEURL", "Waykichain Platform API")


    @JvmField
    var SWAGGER_WABAPI_LICENSE = BaseEnv.env("SWAGGER_WABAPI_LICENSE", "Commercial License, Version 1.0")

    @JvmField
    var SWAGGER_WABAPI_LICENSE_URL = BaseEnv.env("SWAGGER_WABAPI_LICENSE_URL", "http://www.apache.org/licenses/LICENSE-2.0.html")
}