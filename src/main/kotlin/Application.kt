package com.waykichain
import com.waykichain.chainstarter.config.TempConfigEnvironment
import com.waykichain.commons.util.BaseEnv
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration
import org.springframework.cache.annotation.EnableCaching
import springfox.documentation.swagger2.annotations.EnableSwagger2


@SpringBootApplication(exclude = [DataSourceAutoConfiguration::class, HibernateJpaAutoConfiguration::class], scanBasePackages = ["com.waykichain"])
@EnableCaching
@EnableSwagger2
open class Application
fun main(args: Array<String>) {
    TempConfigEnvironment.putConfig("WICC_HOST_IP", "10.0.0.20")
    TempConfigEnvironment.putConfig("WICC_HOST_PORT", "6968")
    TempConfigEnvironment.putConfig("WICC_RPC_USERNAME", "wayki")
    TempConfigEnvironment.putConfig("WICC_RPC_PASSWORD", "admin@123")
    BaseEnv.init()
    SpringApplication.run(Application::class.java, *args)
}