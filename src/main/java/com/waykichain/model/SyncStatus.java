package com.waykichain.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * SyncStatus is used to provide additional context about an implementation&#39;s sync status. It is often used to indicate that an implementation is healthy when it cannot be queried  until some sync phase occurs. If an implementation is immediately queryable, this model is often not populated.
 */
@ApiModel(description = "SyncStatus is used to provide additional context about an implementation's sync status. It is often used to indicate that an implementation is healthy when it cannot be queried  until some sync phase occurs. If an implementation is immediately queryable, this model is often not populated.")

public class SyncStatus  implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty("current_index")
  private Long currentIndex;

  @JsonProperty("target_index")
  private Long targetIndex;

  @JsonProperty("stage")
  private String stage;

  public SyncStatus currentIndex(Long currentIndex) {
    this.currentIndex = currentIndex;
    return this;
  }

  /**
   * CurrentIndex is the index of the last synced block in the current stage.
   * @return currentIndex
  */
  @ApiModelProperty(example = "100", required = true, value = "CurrentIndex is the index of the last synced block in the current stage.")
  @NotNull


  public Long getCurrentIndex() {
    return currentIndex;
  }

  public void setCurrentIndex(Long currentIndex) {
    this.currentIndex = currentIndex;
  }

  public SyncStatus targetIndex(Long targetIndex) {
    this.targetIndex = targetIndex;
    return this;
  }

  /**
   * TargetIndex is the index of the block that the implementation is attempting to sync to in the current stage.
   * @return targetIndex
  */
  @ApiModelProperty(example = "150", value = "TargetIndex is the index of the block that the implementation is attempting to sync to in the current stage.")


  public Long getTargetIndex() {
    return targetIndex;
  }

  public void setTargetIndex(Long targetIndex) {
    this.targetIndex = targetIndex;
  }

  public SyncStatus stage(String stage) {
    this.stage = stage;
    return this;
  }

  /**
   * Stage is the phase of the sync process.
   * @return stage
  */
  @ApiModelProperty(example = "header sync", value = "Stage is the phase of the sync process.")


  public String getStage() {
    return stage;
  }

  public void setStage(String stage) {
    this.stage = stage;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SyncStatus syncStatus = (SyncStatus) o;
    return Objects.equals(this.currentIndex, syncStatus.currentIndex) &&
        Objects.equals(this.targetIndex, syncStatus.targetIndex) &&
        Objects.equals(this.stage, syncStatus.stage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(currentIndex, targetIndex, stage);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SyncStatus {\n");
    
    sb.append("    currentIndex: ").append(toIndentedString(currentIndex)).append("\n");
    sb.append("    targetIndex: ").append(toIndentedString(targetIndex)).append("\n");
    sb.append("    stage: ").append(toIndentedString(stage)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

