package com.waykichain.common;

import com.waykichain.model.Currency;
import com.waykichain.model.NetworkIdentifier;

public class Default {

  public static final Currency CURRENCY = new Currency();
  public static final NetworkIdentifier NETWORK_IDENTIFIER = new NetworkIdentifier();

  static  {
    CURRENCY.symbol("wicc")
        .decimals(8);
  }

  static {
    NETWORK_IDENTIFIER.blockchain("waykichain")
        .network("mainnet");
  }
}
